NodeBolt adds a simple layer of protection to remind admins / editors that for some reason, this node should be handled with a little more care when it comes to editing.  For the node ids specified by the settings page, NodeBolt adds an extra step when editing.

If you are looking for a quick and simple way to protect a few pieces of content from yourself or others, then NodeBolt is for you.

Examples of pages to bolt.
 <ul>
 <li>One off site pages (About, Contact Us, etc.)</li>
 <li><a href='http://drupal.org/project/webform'>webforms</a></li>
 <li><a href='http://drupal.org/project/nodeblock'>Node Blocks</a></li>
</ul>

Example Usecase:
A site has a sign up form, on which the body uses php (thats another discussion) but its hidden since the wysiwyg is enabled.
We bolt that node so an error message appears on the node edit and save page, to prompt the user that for some reason we dont want this node to be readily savable.  Also helpful if you are using the admin account for CRUD operations on site nodes, but need to remind yourself from time to time not to edit something.

For this module, a bolt is simply a validation error by nid, if nid then validation fails.


What I would like to add if people find this useful
 * Custom error messages for each nid
 * Bolt by content type
 * Bolted page listing

